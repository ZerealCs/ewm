use std::{collections::{VecDeque, HashMap}, hash::Hash};

use crate::model::{bsp::BSP, utils::Orientation, window_wrap::WindowWrap, workspace::Workspace, config::Config};

/// Structure a logical behavior for a set of windows and monitors
pub struct WindowsSet<Shell, Output> {
    pub config: Config,
    /// Specified for convertible devices, it should disable touchpad and embedded
    /// keyboard. And enable `touch_interface`
    pub tablet_mode: bool,
    /// All Shells being manage
    pub shells: HashMap<Shell, WindowWrap<Shell, Output>>,
    /// All outputs being manage
    pub outputs: HashMap<Output, Screen<Shell, Output>>,
    /// Workspaces avaiklable
    pub workspaces: Vec<Workspace<Shell, Output>>,
    /// Special kind of windows shared between workspaces
    /// Use relative to scratchpads.
    pub global_windows: VecDeque<WindowWrap<Shell, Output>>,
    /// Defines the arrange of screens
    pub screens: BSP<SplitScreen, Screen<Output, Shell>>,
    /// Shared screen mode
    pub screens_mode: ScreenMode,
}

impl<Shell : Hash + Eq, Output: Hash + Eq> WindowsSet<Shell, Output> {
    pub fn new(config: Config) -> Self {
        Self {
            config,
            tablet_mode: false,
            shells: HashMap::new(),
            outputs: HashMap::new(),
            workspaces: Vec::new(),
            global_windows: VecDeque::new(),
            screens: BSP::empty(),
            screens_mode: ScreenMode::Extended,
        }
    }

    /// Apply this function to every window that is on a screen
    /// it is given needed information about the window to be mapped
    /// TODO:
    pub fn get_windows_from_output<F>(&mut self, output: &Output, apply: F)
    where
        F: FnMut(&mut Shell),
    {
        match self.outputs.get(output) {
            Some(screen) => {
                match self.workspaces.get_mut(screen.workspace) {
                    Some(workspace) => workspace.map_windows(apply),
                    None => ()
                }
            },
            None => ()
        }
    }

    pub fn remove_output(&mut self, output: Output) {
        // TODO

    }

    pub fn window_at_cursor(&self) -> Option<WindowWrap<Shell, Output>> {
        None
    }

    pub fn add_output(&mut self, output: Output) {}

    pub fn focus(&mut self, window: WindowWrap<Shell, Output>) {}

    pub fn insert_shell(&mut self, shell: Shell) {
        // WindowWrap::new(shell)
        // shells
    }

    pub fn show(&mut self, shell: Shell) {}

    /// The same as minimize maybe take a look after internal logic gets better
    pub fn hide(&mut self, shell: Shell) {}

    pub fn minimize(&mut self, shell: Shell) {}

    pub fn maximize(&mut self, shell: Shell) {}

    pub fn fullscreen(&mut self, shell: Shell) {}

    pub fn remove_shell(&mut self, shell: Shell) {}
}

/// Behavior of screen with workspaces scheme
pub enum ScreenMode {
    /// This set each screen should have one workspace linked
    Extended,
    /// All screen share the same workspace but it is extended between them
    ExtendedWorkspace,
    /// Duplicate the output on the screens
    Duplicate,
}

pub struct Screen<Output, Shell> {
    /// Update the internal compositor GUI to get a better touch interface
    /// It shouldn't update DPI in a global manner.
    touch_interface: bool,
    /// Access to output info handler
    output: Output,
    /// Docks configuration on the screen
    docks: WindowWrap<Shell, Output>,
    /// Workspace being showed
    workspace: usize,
}

/// Define interactions between screen pointers movements
pub struct SplitScreen {
    orientation: Orientation,
    gap: ScreenGap,
    mouse_translation: ScreenMouseTranslation,
}

/// Behavior on crossover screens with the mouse
pub enum ScreenGap {
    /// Mouse get stuck on screen until passed exterior gap, this gap is supposed to reset every
    /// time that cursor move to the center of the screen.
    MouseGap(usize),
    /// Mouse get stuck in the screen when it is inside, just an action could let move mouse to
    /// other screen
    Trapped,
    /// Normal behavior, in function of screen positions
    Normal,
}

/// Behavior with the position of monitors
pub enum ScreenMouseTranslation {
    /// On mouse crossover screen it keep proportional
    ///    -----
    ///    |   |
    ///    |   |
    ///  ----|----
    ///  |   |   |
    ///  |   |   |
    ///  ---------
    Proportional,
    /// TODO:Needs some better definition
    /// Displacement of screen in function of its orientation
    Fixed(usize),
}
