
use std::{rc::{Rc, Weak}, cell::RefCell};


pub enum Node<E, L: Eq>{
    Branch {
        parent: Option<Weak<RefCell<Node>>>,
        left: Box<Node>,
        right: Box<Node>,
        val: E,
    },
    Leaf {
        parent: Option<Weak<RefCell<Node>>>,
        val: L,
    }
    Empty {
        parent: Option<Weak<RefCell<Node>>>,
    },
}

pub struct Tree<E, L> {
    root: Rc<RefCell<Node<E, L>>>
}

impl<E, L: Eq> Tree<E, L> {

    pub fn new() -> Self {
        Self {
            root: Rc::new(RefCell::new(Node::Empty { parent: None }))
        }
    }

    pub fn add_balanced(leave: L) {
        match self.root {
            Node::
        }
    }

    pub fn add_next_to_with_ref(&mut self, new_leave: L, inserted_leave: Weak<RefCell<Node<E,L>>>) {
        // create Branch
        let new_branch = Edge::new_branch();
        let parent = inserted_leave.parent;
        if parent.left == inserted_leave {
            parent.left = new_branch
        }
        else {
            parent.right = new_branch
        }
        new_branch.left = new_leave;
        new_branch.right = inserted_leave;
    }

    pub fn swap_leaves()

    pub fn get_leave_ref(&self, leave: L) -> Option<Weak<RefCell<Node<E,L>>>> {
        let mut current_nodes = [self.root];
        let mut found = None;
        while found.is_none() {
            if let Some(node) = current_nodes.first() {
                match node {
                    Node::Branch { left, right,.. } => {
                        current_nodes.push_back(left);
                        current_nodes.push_back(right);
                    },
                    Node::Leaf { val, .. } => {
                        if val == leave {
                            find = node.weak();
                        }
                    },
                    Node::Empty { .. } => {},
                }
            }
        }
        found
    }
}
